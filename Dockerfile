FROM ubuntu:18.04

RUN apt-get update
RUN apt-get -y install openssl

ENTRYPOINT openssl req -nodes -newkey rsa:2048 -keyout /certs-server/server.key.pem -out /certs-server/server.csr.pem -subj "/C=US/ST=Colorado/L=Boulder/O=Dropworks, Inc./OU=Continuum/CN=$DEVICE_NAME" && \
    openssl x509 -req -days 365 -CA /certs-ca/dropworks_ca.dev.cert.pem -CAkey /certs-ca/dropworks_ca.dev.key.pem -passin pass:$CERT_PASS -CAcreateserial -addreject emailProtection -addreject clientAuth -addtrust serverAuth -in /certs-server/server.csr.pem -out /certs-server/server.cert.pem && \
    openssl x509 -in /certs-server/server.cert.pem -out /certs-server/server.cert.pem
